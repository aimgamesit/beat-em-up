﻿using UnityEngine;

public class PlayerSpawnPoint : MonoBehaviour
{
    public UIHUDHealthBar uIHUDHealthBar;

    void Awake()
    {
        GameObject obj = loadPlayer(AIMGameData.Instance.playerCharList[PrefsManager.Instance.GetInt(PrefsManager.PLAYER_INDEX, 0)].charPrefab, "Player_");
        HealthSystem healthSystem = obj.GetComponent<HealthSystem>();
        healthSystem.uIHUDHealthBar = uIHUDHealthBar;
    }

    //load a player prefab
    GameObject loadPlayer(GameObject playerPrefab, string name = "")
    {
        GameObject player = Instantiate(playerPrefab) as GameObject;
        player.transform.position = transform.position;
        player.name = "Player_" + name;
        return player;
    }
}