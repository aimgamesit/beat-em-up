﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class KillBox : MonoBehaviour {

	public bool RestartOnPlayerKill;
	public bool RestartOnEnemyKill;

	//destroy everything that enters this trigger
	void OnTriggerEnter(Collider coll){

		//restart level on player kill
		if(RestartOnPlayerKill && coll.CompareTag("Player")) RestartLevel();

		//restart level on enemy kill
		if(RestartOnEnemyKill && coll.CompareTag("Enemy")) RestartLevel();

		//destroy gameobject
		Destroy (coll.gameObject);
	}

	//restart level
	void RestartLevel(){
		//load level
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}
}