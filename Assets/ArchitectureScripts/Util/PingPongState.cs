﻿using UnityEngine;
using System.Collections;

namespace com.aquimo.football.statemachine
{
	[SharedBetweenAnimators ()]
	public class PingPongState : StateMachineBehaviour
	{
		public const string PING_PONG_PARAM = "pingPong";

		// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
		override public void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{

		}

		// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
		override public void OnStateUpdate (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			//OPT
			if (stateInfo.normalizedTime <= 0.01) {
				animator.SetFloat (PING_PONG_PARAM, 1);
			} else if (stateInfo.normalizedTime >= 0.99f) {
				animator.SetFloat (PING_PONG_PARAM, -1);
			}
		}

		//OnStateExit is called when a transition ends and the state machine finishes evaluating this state
		override public void OnStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
		override public void OnStateMove (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
	
		}

		// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
		override public void OnStateIK (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
	
		}
	}
}