﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace com.aquimo.football.api
{
	public static class EncryptionHandler
	{
		//Encryption using Rijindael algo. Encryptes all POST params JSON and Device Id and the payload Key.
		public static String EncryptIt (String s)
		{	
			String result;//

			using (RijndaelManaged rijn = new RijndaelManaged ()) {
				rijn.Mode = CipherMode.ECB;
				rijn.Padding = PaddingMode.Zeros;//

				using (MemoryStream msEncrypt = new MemoryStream ()) {
					using (ICryptoTransform encryptor = rijn.CreateEncryptor (APIConst.KEY, APIConst.IV)) {
						using (CryptoStream csEncrypt = new CryptoStream (msEncrypt, encryptor, CryptoStreamMode.Write)) {
							using (StreamWriter swEncrypt = new StreamWriter (csEncrypt)) {
								swEncrypt.Write (s);
							}
						}
					}
					result = Convert.ToBase64String (msEncrypt.ToArray ());
				}
				rijn.Clear ();
			}//

			return result;
		}
	
		// Not used till now. For furhter use if the response received is Encrypted.
		public static String DecryptIt (String s)
		{
			String result;//

			using (RijndaelManaged rijn = new RijndaelManaged ()) {

				rijn.Mode = CipherMode.ECB;
				rijn.Padding = PaddingMode.Zeros;//
		
				using (MemoryStream msDecrypt = new MemoryStream (Convert.FromBase64String (s))) {
					using (ICryptoTransform decryptor = rijn.CreateDecryptor (APIConst.KEY, APIConst.IV)) {
						using (CryptoStream csDecrypt = new CryptoStream (msDecrypt, decryptor, CryptoStreamMode.Read)) {
							using (StreamReader swDecrypt = new StreamReader (csDecrypt)) {
								result = swDecrypt.ReadToEnd ();
							}
						}
					}
				}
				rijn.Clear ();
			}

			result = result.TrimEnd ('\0');//OPT 

			return result;

		}

	}
}