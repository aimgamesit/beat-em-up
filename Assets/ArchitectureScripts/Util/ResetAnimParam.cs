﻿using UnityEngine;
using System.Collections;

public class ResetAnimParam : StateMachineBehaviour
{
	public ParamInfo[] paramArray;

	public override void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		resetParam (animator);
		base.OnStateEnter (animator, stateInfo, layerIndex);
	}

	void resetParam (Animator animator)
	{
		foreach (var item in paramArray) {
			switch (item.paramType) {
			case ParamInfo.ParamType.Bool:
				animator.SetBool (item.name, item.value >= 0);
				break;

			case ParamInfo.ParamType.Float:
				animator.SetFloat (item.name, item.value);
				break;

			case ParamInfo.ParamType.Integer:
				animator.SetInteger (item.name, ((int)item.value));
				break;
			}
		}
	}

	[System.Serializable]
	public class ParamInfo
	{
		public string name;

		public ParamType paramType = ParamType.Integer;

		[Tooltip ("Negative value in bool considered as False and In Integer it will be rounded off.")]
		public float value = -1.0f;

		public enum ParamType
		{
			Integer,
			Float,
			Bool
		}
	}

}
