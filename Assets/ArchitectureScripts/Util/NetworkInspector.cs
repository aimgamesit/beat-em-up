using UnityEngine;
using com.aquimo.util;
using System.Collections;
using UnityEngine.Events;

namespace com.aquimo.util
{
	/// <summary>
	/// Util class for network related Work.
	/// </summary>
	
	#region SingleTon Pattern.
	public partial class NetworkInspector : MonoBehaviour , IManager
	{
		static NetworkInspector _Instance;

		public static NetworkInspector Instance {
			get {
				if (_Instance == null) {
					GameObject gObj = new GameObject ("NetworkInspector");
					_Instance = gObj.AddComponent<NetworkInspector> ();
				}
				return _Instance;
			}
		}

		void OnDestroy ()
		{
			_Instance = null;
		}
	}
	#endregion

	//Public Methods
	public partial class NetworkInspector : MonoBehaviour
	{
		public void isInternetWorking (UnityAction<bool> onComplete)
		{
			StartCoroutine (isInternetConnected (onComplete));
		}

		public void onInternetWorking (UnityAction onComplete)
		{
			StartCoroutine (onInternetConnected (onComplete));
		}

		public void downloadTextFile (string url, UnityAction<string> onComplete = null, int numTry = -1)
		{
			StartCoroutine (loadTextFile (url, onComplete, numTry));
		}
	}

	//Implementation
	public partial class NetworkInspector : MonoBehaviour
	{

		IEnumerator isInternetConnected (UnityAction<bool> onComplete = null)
		{
			using (WWW www = new WWW ("www.google.com")) {
				yield return www;
				onComplete.Invoke (www.error == null);
			}
		}

		IEnumerator onInternetConnected (UnityAction onComplete = null)
		{

			for (int i = 0;; i++) {
				using (WWW www = new WWW ("www.google.com")) {
					yield return www;
					if (www.error != null) {
						yield return null;
						continue;
					}
				}

				if (onComplete != null) {
					onComplete.Invoke ();
				}
				yield break;
			}
		}

		// -n for infinity , n for number of retries.
		IEnumerator loadTextFile (string url, UnityAction<string> onComplete = null, int numTry = -1)
		{
			for (;;) {
				using (WWW www = new WWW (url)) {
					yield return www;
					if (www.error != null) {
						if (numTry == 0) {
							onComplete.Invoke (null);
							yield break;
						}
						yield return StartCoroutine (isInternetConnected ());
						numTry = numTry > 0 ? numTry-- : numTry;
						continue;
					}
					onComplete (www.text);
					yield break;
				}
			}
		}

	}

}