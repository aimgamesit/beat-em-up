﻿using UnityEngine;
using System.Threading;
using UnityEngine.Events;
using System.Collections.Generic;

namespace com.aquimo.util
{

	public class Loom : MonoBehaviour
	{
		static Queue<UnityEvent> mainThreadQueue = new Queue<UnityEvent> ();
	
		// Update is called once per frame
		void Update ()
		{
			if (Monitor.TryEnter (mainThreadQueue)) {
				try {
					while (mainThreadQueue.Count > 0) {
						mainThreadQueue.Dequeue ().Invoke ();
					}
				} finally {
					Monitor.Exit (mainThreadQueue);
				}
			}
		}

		public static void QueueOnMainThread (UnityEvent mainThreadDelegate)
		{
			Monitor.Enter (mainThreadQueue);
			try {
				mainThreadQueue.Enqueue (mainThreadDelegate); 
			} finally {
				Monitor.Exit (mainThreadQueue);
			}
		}

	}

}