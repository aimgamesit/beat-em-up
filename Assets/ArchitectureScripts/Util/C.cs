using System;
using System.IO;
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using com.aquimo.data;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace com.aquimo.util
{
	#region All Static and Const Variables.
	public static partial class C
	{
		public const int DEFAULT = -1;

		public const string DEFAULT_SHADER = "Standard";

		public const float YardInMeter = 0.9144f;

		public const float MeterInYard = 1.03961f;

		public static string OS_APP_DATA_PATH {
			get {
				string path = "";
				if (System.Environment.OSVersion.ToString ().Contains ("Windows")) {
					path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.ApplicationData);
				} else {
					path = Path.Combine (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal), "Library");
				}
				return path;
			}
		}

		//		public static float GetRoundValue (float value, float round = 100.0f)
		//		{
		//			return ((int)(value * round)) / round;
		//		}


	}
	#endregion

	#region All Static Methods.
	public static partial class C
	{
		#region Runtime Methods


		public static void ClearAllChilds(this Transform transform)
			{
				foreach (Transform child in transform) {
					GameObject.Destroy(child.gameObject);
				}
				
			}


		public static void ToggleObjects (GameObject activeObj, GameObject deActiveObj)
		{
			if (activeObj != null) {
				activeObj.SetActive (true);
			} else {
				
				Debug.LogWarning (" DeActiveobj is null while toggle Object State. ");
			}
			
			//
			if (deActiveObj != null) {
				deActiveObj.SetActive (false);
			} else {
				Debug.LogWarning (" Activeobj is null while toggle Object State. ");
			}
		}

		public static string GetOrdinalNumber (int value)
		{
			string suffix = (value == 1) ? "st" : (value == 2) ? "nd" : (value == 3) ? "rd" : "th";
			return value + suffix;
		}

		public static void ToggleObjects (bool objState, params GameObject[] gameObjs)
		{
			if (gameObjs != null) {
				foreach (GameObject gameObj in gameObjs) {
					if (gameObj != null) {
						gameObj.SetActive (objState);
					} else {
						Debug.LogWarning (" GameObject is null while toggle Object into  " + objState);
					}
				}				
			} else {
				Debug.LogWarning (" Gameobj Array is null while toggleObjects into  " + objState);
			}
		}

		public static string GetUtf8String (byte[] msg)
		{
			try {
				return System.Text.Encoding.UTF8.GetString (msg);
			} catch (Exception e) {
				//Debug.LogError (" Exception while encoding byte[] to string " + e.ToString ());
				return null;
			}
		}

		public static byte[] GetUtf8Bytes (string msg)
		{
			try {
				return System.Text.Encoding.UTF8.GetBytes (msg);
			} catch (Exception e) {
				//Debug.LogError (" Exception while encoding string to byte[] " + e.ToString ());
				return null;
			}
		}

		public static string GetTimeStamp ()
		{
			return System.DateTime.Now.ToString ("yyyyMMddHHmmss");
		}

		public static string ColorToHex (Color32 color)
		{
			return string.Concat ("#", color.r.ToString ("X2"), color.g.ToString ("X2"), color.b.ToString ("X2"));
		}

		public static float GetRangeValue (float normalValue, Vector2 range)
		{
			return GetRangeValue (normalValue, range.x, range.y);
		}

		public static float GetRangeValue (float normalValue, float min, float max)
		{
			return min + (normalValue * (max - min));
		}

		public static string GetMoneyFormat (double money)
		{
			if (money >= 1000000000)
				return  (int)(money / 1000000000) + "B";

			if (money >= 1000000)
				return (int)(money / 1000000) + "M";

			if (money >= 1000)
				return (int)(money / 1000) + "K";

			return "" + money;
		}

		public static IEnumerator LerpObj (Transform targetObj, Vector3 target, float speed)
		{
			float startTime = Time.time;
			Vector3 startPosition = targetObj.position;
			float journeyLength = Vector3.Distance (targetObj.position, target);
			
			while (true) {
				if (Vector3.Distance (target, targetObj.position) < 0.01f) {
					targetObj.position = target;
					break;   
				}
				float distCovered = (Time.time - startTime) * speed;
				float fracJourney = distCovered / journeyLength;
				targetObj.position = Vector3.Lerp (startPosition, target, fracJourney);
				yield return new WaitForFixedUpdate ();
			}
		}

		public static int[] GetIntArray (string value)
		{
			if (string.IsNullOrEmpty (value)) {
				Debug.LogWarning ("Value is Null or Empty ");
				return new int[]{ 0, 0, 0 };
			}
			try {
				return value.Select (c => Convert.ToInt32 (c.ToString ())).ToArray ();
			} catch (System.Exception e) {
				Debug.LogWarning ("Value Exception " + e);
				return new int[]{ 0, 0, 0 };
			}
		}

		public static Vector3 GetVector3 (string value, char seprator = ',')
		{
			float[] values = C.GetFloatValues (value, seprator);

			if (values == null || values.Length < 3) {
				Debug.LogWarning ("Get Vector3 is one");
				return Vector3.one;
			}

			return new Vector3 (values [0], values [1], values [2]);
		}

		public static float[] GetFloatValues (string value, char seprator = ',')
		{
			if (string.IsNullOrEmpty (value)) {
				Debug.LogWarning ("Value is Null or Empty ");
				return null;
			}

			try {
				string[] values = value.Split (seprator);

				int count = values.Length;
				float[] mValues = new float[count];
				for (int i = 0; i < count; i++) {
					mValues [i] = float.Parse (values [i]);
				}

				return mValues;
			} catch (System.Exception e) {
				Debug.LogWarning ("Value Exception " + e);
			}

			return null;
		}

		public static string GetPath (string path, string name)
		{
			string[] childs = Directory.GetDirectories (path);
			foreach (var childPath in childs) {
				string childName = new DirectoryInfo (childPath).Name;
				if (childName.Contains (name)) {
					return childPath;
				}
			}
			return null;
		}

		public static Vector3 ArrangeChilds (Transform parent)
		{
			Vector3 position = Vector3.zero;
			foreach (Transform child in parent) {
				child.Reset ();
				Renderer mRenderer = child.GetComponent<MeshRenderer> ();
				if (mRenderer != null) {
					child.localPosition = position;
					position += Vector3.up * mRenderer.bounds.size.y;
				}
			}
			
			return position;
		}

		public static bool LoadAssetDataInPlayerPrefs (string path, string key, bool checkPlayerPrefs = true)
		{
			if (checkPlayerPrefs && !string.IsNullOrEmpty (PlayerPrefs.GetString (key)))
				return false;
			
			TextAsset authAsset = (TextAsset)Resources.Load (path, typeof(TextAsset));
			if (authAsset == null)
				return false;
			
			string data = authAsset.ToString ();
			PlayerPrefs.SetString (key, data);//
			
			Resources.UnloadAsset (authAsset);
			return true;
		}

		public static bool IsIndex (int limit, int index)
		{
			return  (limit > 0 && index >= 0 && index < limit);
		}

		public static int GetNextCyclicIndex (int arrayLength, int currentIndex)
		{
			return (currentIndex >= (arrayLength - 1) || currentIndex < 0) ? 0 : ++currentIndex;
		}

		public static T LoadScriptableObject<T> () where T : BData<T>
		{
			return LoadScriptableObject<T> (typeof(T).Name, delegate(T obj) {
				return obj.getResrcPath ();
			});
		}

		public static T LoadScriptableObject<T> (string path) where T : ScriptableObject
		{
			return LoadScriptableObject (typeof(T).Name, delegate(T obj) {
				return path;
			});
		}

		public static T LoadScriptableObject<T> (string typeName, string path) where T : ScriptableObject
		{
			return LoadScriptableObject (typeName, delegate(T obj) {
				return path;
			});
		}

		static T LoadScriptableObject<T> (string typeName, GameAction<string,T> getPath) where T : ScriptableObject
		{
			T instance = Resources.Load (typeName) as T;//
			
			if (instance == null) {
				instance = ScriptableObject.CreateInstance<T> ();
				#if UNITY_EDITOR
				CreateAsset (instance, typeName, getPath.Invoke (instance));
				#endif
			}
			
			return instance;
		}

		#endregion

		#region UI Mehtods

		public static void RemoveOnClickListeners (params UnityEngine.UI.Button[] buttons)
		{
			foreach (var button in buttons) {
				button.onClick.RemoveAllListeners ();
			}
		}

		#endregion

		#region Unity Editor Methods

		#if UNITY_EDITOR
		public static void UpdateFileText (string filePath, string text)
		{
//			string assetPath = Path.Combine (Application.dataPath, filePath);
//			File.WriteAllText (assetPath, text);
		}

		public static string GetAssetPath (string path, string filter)
		{
			string[] files = AssetDatabase.FindAssets (filter, new string[]{ path });
			return ((files != null) && (files.Length > 0)) ? files [0] : "";
		}

		public static T LoadABScriptableObject<T> (string path) where T : ScriptableObject //AB AssetBundle
		{
			T mObj = AssetDatabase.LoadAssetAtPath<T> (path);
			
			if (mObj == null) {
				mObj = ScriptableObject.CreateInstance<T> ();
				AssetDatabase.CreateAsset (mObj, path);
			}
			
			return mObj;
		}

		public static void CreateAsset (UnityEngine.Object instance, string className, string path)
		{
			string folderName = "Resources";//

			string folderPath = Path.Combine ("Assets", path);
			string directoryPath = Path.Combine (Application.dataPath, path);
			directoryPath = Path.Combine (directoryPath, folderName);
						
			if (!Directory.Exists (directoryPath)) {
				AssetDatabase.CreateFolder (folderPath, folderName);
			}
			            
			string assetPath = Path.Combine (folderPath, folderName);
			assetPath = Path.Combine (assetPath, string.Concat (className, ".asset"));
			AssetDatabase.CreateAsset (instance, assetPath);
			AssetDatabase.SaveAssets ();
		}

		public static void CreatFolderPath (FolderPath folderPath, string dirPath)
		{
			if (dirPath != null) {
				if (Directory.Exists (dirPath)) {

					string[] childs = Directory.GetDirectories (dirPath);
					int childCount = childs.Length;
					folderPath.childs = new FolderPath[childCount];//

					for (int i = 0; i < childCount; i++) {
						string childPath = Path.Combine (folderPath.path, new DirectoryInfo (childs [i]).Name);
						folderPath.childs [i] = new FolderPath (childPath);//

						CreatFolderPath (folderPath.childs [i], childs [i]);
					}
				}     
			}
		}

		public static void CreateDirectory (FolderPath folderPath, string newDirPath = null, bool isGitFile = false)
		{
			newDirPath = newDirPath ?? Application.dataPath;
			newDirPath = Path.Combine (newDirPath, folderPath.path);

			if (!Directory.Exists (newDirPath)) {
				Directory.CreateDirectory (newDirPath);
				if (isGitFile) {
					string gitFile = Path.Combine (newDirPath, ".gitignore");
					File.Create (gitFile).Close ();
					File.SetAttributes (
						gitFile, 
						FileAttributes.Archive |
						FileAttributes.Hidden |
						FileAttributes.ReadOnly
					);
				}
			}

			if (folderPath.childs != null) {
				int subFolderCount = folderPath.childs.Length;
				for (int i = 0; i < subFolderCount; i++) {
					CreateDirectory (folderPath.childs [i], newDirPath, isGitFile); 	
				}
			}
		}

		public static void CopyFilesAndDirectory (string fromPath, string toPath)
		{
			string[] directories = Directory.GetDirectories (fromPath);
			string[] files = Directory.GetFiles (fromPath);

			foreach (string filePath in files) {
				bool isHidden = ((File.GetAttributes (filePath) & FileAttributes.Hidden) == FileAttributes.Hidden);
				if (!filePath.Contains (".meta") && !isHidden) {
					string fileName = new FileInfo (filePath).Name;
					File.Copy (filePath, Path.Combine (toPath, fileName), true);	
				}
			}
			
			foreach (var dirPath in directories) {
				string dirName = new DirectoryInfo (dirPath).Name;
				string newToPath = Path.Combine (toPath, dirName);
				
				if (!Directory.Exists (newToPath)) {
					Directory.CreateDirectory (newToPath);
				}
				
				CopyFilesAndDirectory (dirPath, newToPath);
			}
		}
		#endif

		#endregion
	}
	#endregion


	public class FolderPath
	{
		public  string path;
		public FolderPath[] childs;

		public FolderPath (string path)
		{
			this.path = path;
		}
	}

}

