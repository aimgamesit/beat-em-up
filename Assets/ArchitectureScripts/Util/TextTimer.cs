﻿using UnityEngine;
using UnityEngine.UI;

namespace com.aquimo.util
{
	[RequireComponent (typeof(Text))]
	public class TextTimer : MonoBehaviour
	{
		Text timerText;

		float timerCount = 0;

		void OnEnable ()
		{
			timerCount = 0;
			timerText = GetComponent<Text> ();
		}

		// Update is called once per frame
		void Update ()
		{
			timerCount += Time.deltaTime;
			timerText.text = timerCount.ToString ("0.0"); //2dp Number
		}
	}
}