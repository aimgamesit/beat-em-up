﻿using UnityEngine;

public class TouchDestroy : MonoBehaviour
{
	public string destroyerTag;

	public float delayTime;

	void OnCollisionEnter (Collision collision)
	{
		if (string.IsNullOrEmpty (destroyerTag))
			return;

		if (collision.collider.CompareTag (destroyerTag))
			Invoke ("destroyObject", delayTime);
	}

	void destroyObject ()
	{
		Destroy (gameObject);
	}
}
