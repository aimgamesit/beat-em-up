using UnityEngine;
using UnityEngine.Events;

namespace com.aquimo.util
{
	public class DisableGameObjectEvents : MonoBehaviour
	{
		public UnityEvent gameObjectEvent;
		
		void OnDisable ()
		{
			gameObjectEvent.Invoke ();
		}
	}
	

	
	
}