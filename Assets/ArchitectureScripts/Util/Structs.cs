﻿using UnityEngine;

namespace com.aquimo.util
{
	[System.Serializable]
	public struct WorldInfo
	{
		public Vector3 position;
		public Quaternion rotation;
		public Vector3 scaling;

		public WorldInfo (Vector3 position)
		{
			this.position = position;
			this.scaling = Vector3.one;
			this.rotation = Quaternion.identity;
		}

		public WorldInfo (Transform target)
		{
			this.position = target.position;
			this.scaling = target.localScale;
			this.rotation = target.rotation;
		}

		public WorldInfo (Vector3 position, Vector3 scaling, Quaternion rotation)
		{
			this.position = position;
			this.scaling = scaling;
			this.rotation = rotation;
		}

		public bool equallTo (Transform obj)
		{
			return obj.position == position && obj.rotation == rotation && obj.localScale == scaling;
		}
	}

	[System.Serializable]
	public struct EulerWorldInfo
	{
		public Vector3 position;
		public Vector3 rotation;
		public Vector3 scaling;

		public EulerWorldInfo (Vector3 position)
		{
			this.position = position;
			this.scaling = Vector3.one;
			this.rotation = Vector3.zero;
		}

		public EulerWorldInfo (Transform target)
		{
			this.position = target.position;
			this.scaling = target.localScale;
			this.rotation = target.rotation.eulerAngles;
		}

		public EulerWorldInfo (Vector3 position, Vector3 scaling, Vector3 rotation)
		{
			this.position = position;
			this.scaling = scaling;
			this.rotation = rotation;
		}

		public bool equallTo (Transform obj)
		{
			return obj.position == position && obj.rotation.eulerAngles == rotation && obj.localScale == scaling;
		}
	}

	[System.Serializable]
	public struct EulerInfo
	{
		public Vector3 position;
		public Vector3 rotation;

		public EulerInfo (Vector3 position)
		{
			this.position = position;
			this.rotation = Vector3.zero;
		}

		public EulerInfo (Transform target)
		{
			this.position = target.position;
			this.rotation = target.rotation.eulerAngles;
		}

		public EulerInfo (Vector3 position, Vector3 rotation)
		{
			this.position = position;
			this.rotation = rotation;
		}


		public bool equallTo (Transform obj)
		{
			return obj.position == position && obj.rotation.eulerAngles == rotation;
		}

	}

	[System.Serializable]
	public struct RangeInfo
	{
		public int min, max;

		[Range (0, 1)]
		public float value;

		public float rangeValue {
			get {
				return C.GetRangeValue (value, min, max);
			}
		}

		public RangeInfo (int min, int max, float value)
		{
			this.min = min;
			this.max = max;

			if (value > 1 || value < 0)
				Debug.LogWarning ("Range value is not normalized " + value);
			this.value = Mathf.Clamp (value, 0, 1);
		}
	}

	[System.Serializable]
	public struct BarInfo
	{
		public int min, max;
		public int value;

		public BarInfo (int min, int max, int value)
		{
			this.min = min;
			this.max = max;
			this.value = value;
		}
	}

	[System.Serializable] 
	public struct DayTime
	{
		public Day day;

		[Range (0, 24)]
		public int time;

		public DayTime (Day day, int time)
		{
			this.day = day;
			this.time = time;
		}

		public static DayTime Current {
			get {
				int time = 0;
				int.TryParse (System.DateTime.Now.ToString ("HH"), out time);
				return new DayTime (((Day)((int)System.DateTime.Now.DayOfWeek)), time);
			}
		}

		public int dayValue {
			get {
				return (int)day;
			}
		}
	}

	[System.Serializable]
	public struct TimeStamp
	{
		public System.DateTime startTime;

		public System.DateTime endTime;

		public TimeStamp (System.DateTime startTime, System.DateTime endTime)
		{
			this.startTime = startTime;
			this.endTime = endTime;
		}
	}

	[System.Serializable]
	public enum Day
	{
		Sunday = 0,
		Monday = 1,
		Tuesday = 2,
		Wednesday = 3,
		Thursday = 4,
		Friday = 5,
		Saturday = 6,
		AnyDay = -1
	}

	[System.Serializable]
	public enum CoinOutPut
	{
		Random = 0,
		Head = 1,
		Tail = 2
	}

}