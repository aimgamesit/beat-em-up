﻿using UnityEngine;
using System.Collections;

namespace com.aquimo.util
{
	//TEMP NOT Complete
	public class SyncRotationPosition : MonoBehaviour
	{
		public float rValue, pValue;
		public Vector3 rDirection, pDirection;
	
		// Update is called once per frame
		void LateUpdate ()
		{
			float result = (pValue / rValue) * getDirectionValue ();
			Vector3 mPosition = transform.position + new Vector3 (pDirection.x + result, pDirection.y + result, pDirection.z + result);
			transform.position = Vector3.Lerp (transform.position, mPosition, Time.deltaTime);
		}

		float getDirectionValue ()
		{
			if (rDirection.x == 1)
				return transform.rotation.eulerAngles.x;

			if (rDirection.y == 1)
				return transform.rotation.eulerAngles.y;

			if (rDirection.z == 1)
				return transform.rotation.eulerAngles.z;

			return 0;
		}
	}
}