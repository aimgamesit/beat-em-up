﻿using UnityEngine;
using System.Collections;

namespace com.aquimo.util
{
	public partial class Toast : MonoBehaviour
	{
		static Toast _Instance;

		public static Toast Instance {
			get {
				if (_Instance == null) {
					GameObject gObj = new GameObject ("Toast");
					_Instance = gObj.AddComponent<Toast> ();
				}
				return _Instance;
			}
		}

		void OnDestroy ()
		{
			_Instance = null;
		}
	}

	public partial class Toast : MonoBehaviour
	{
		public const int LENGTH_LONG = 5;
		public const int LENGTH_SHORT = 3;

		private static GUIContent guiContent;
		private static float m_time;
		private static int m_depth;

		private Texture2D m_textureBG;

		private static float TransitionIn { get; set; }

		protected GUIStyle StyledText {
			get {
				if (m_textStyle == null) {
					m_textStyle = new GUIStyle (GUI.skin.label);
					m_textStyle.fontSize = 21;
					m_textStyle.fixedHeight = 30;
					m_textStyle.richText = true;
					m_textStyle.fontStyle = FontStyle.Bold;
					m_textStyle.normal.textColor = new Color (0.8f, 0.8f, 0.8f);
				}
				return m_textStyle;
			}
		}

		protected GUIStyle m_textStyle;

		void Awake ()
		{
			m_textureBG = new Texture2D (1, 1, TextureFormat.RGBA32, false);
			m_textureBG.SetPixel (0, 0, new Color (0.0f, 0.0f, 0.0f, 0.75f));
			m_textureBG.Apply ();
		}

		void OnGUI ()
		{
			if (m_time <= 0) {
				return;
			}
			if (m_depth == 0) {
				m_depth = GUI.depth - 1;
			}
			GUI.depth = m_depth;
			if (TransitionIn <= 1) {
				GUI.color = Color.Lerp (Color.clear, GUI.color, TransitionIn);
				TransitionIn += Time.deltaTime;
			} else {
				m_time -= Time.deltaTime;
				GUI.color = Color.Lerp (Color.clear, GUI.color, m_time);
			}

			Rect rect = GUILayoutUtility.GetRect (guiContent, StyledText);
			Rect argRect = new Rect (
				               (Screen.width - rect.width) * 0.5f,
				               (Screen.height - rect.height) * 0.9f,
				               rect.width,
				               rect.height
			               );

			GUI.DrawTexture (new Rect (
				argRect.xMin - 5,
				argRect.yMin - 5,
				argRect.width + 10,
				argRect.height + 10
			), m_textureBG);

			GUI.Label (argRect, guiContent, StyledText);
		}

		//Call from UI
		public void ShowText (string text)
		{
			ShowText (text, LENGTH_SHORT);
		}

		public void ShowText (string text, int time)
		{
			guiContent = new GUIContent (text);
			m_time = time;
			TransitionIn = m_depth = 0;
		}
	}
}
