﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Text))]
public class TextAnim : MonoBehaviour
{
	public float time = 1;

	Text mText;
	string msg;

	float updateTime = 0;

	// Use this for initialization
	void OnEnable ()
	{
		mText = GetComponent<Text> ();
		if (mText != null)
			msg = mText.text;
	}
	
	// Update is called once per frame
	void Update ()
	{ 
		if (mText == null)
			return;

		if (updateTime < (time / 2))
			mText.text = msg + ". ";
			
		if (updateTime > (time / 2) && updateTime < time)
			mText.text = msg + "...";

		if (updateTime >= time) 
			mText.text = msg + "  ";

		if (updateTime >= (time + time/2)) 
			updateTime = 0;
			
		updateTime += Time.deltaTime;
	}

	void OnDisable ()
	{
		if (mText != null)
			mText.text = msg;
	}

}
