﻿using UnityEngine;
using UnityEngine.Events;

namespace com.aquimo.util
{
	//TEMP Not Proper Recheck Code
	[System.Obsolete ("Not Done yet.")]
	public class LerpObject : MonoBehaviour
	{
		const float PATH_PRECISION = 1.0f;

		Vector3 target;
		UnityAction onComplete;
		//CallBacks

		float value;
		float timeCount = 0;
		public float delay = 0;
		public float speed = 1;


		public void lerp (Vector3 value, float delay = 0, UnityAction onComplete = null)
		{
			this.value = value.z;
			Vector3 mTarget = new Vector3 (transform.position.x + value.x, transform.position.y + value.y, transform.position.z + value.z);
			setTarget (mTarget, delay, onComplete);
		}

		public void setTarget (Vector3 target, float delay = 0, UnityAction onComplete = null)
		{
			enabled = true;
			this.delay = delay;
			this.timeCount = 0;
			this.target = target;
			this.onComplete = onComplete;
		}

		void LateUpdate ()
		{
			float distance;
			if (value < 0)
				distance = transform.position.z - target.z;//
			else
				distance = target.z - transform.position.z;//
				
			if (distance > PATH_PRECISION && timeCount <= 0) {
//				transform.position = Vector3.MoveTowards (transform.position, target, speed * Time.deltaTime);//TEMP
				transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z + ((value > 0 ? 1 : -1) * speed * Time.deltaTime));//TEMP
			} else {

				if (timeCount >= delay) {
					enabled = false;
					if (onComplete != null) {
						onComplete.Invoke ();
					}
				} else {
					timeCount += Time.deltaTime;
				}
				
			}
		}

		void OnDisable ()
		{
			enabled = false;
		}


	}
}