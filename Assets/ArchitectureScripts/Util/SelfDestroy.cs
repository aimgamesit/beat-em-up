﻿using UnityEngine;

namespace com.aquimo.util
{
	public class SelfDestroy : MonoBehaviour
	{
		float cTime = 0;
		public float time = 1.0f;

		void Update ()
		{
			if (cTime > time) {
				Destroy (gameObject);
			}

			cTime += Time.deltaTime;
		}
	}
}