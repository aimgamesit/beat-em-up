﻿using UnityEngine;
using System.Collections.Generic;

namespace com.aquimo.util
{
	public class SmoothLookAt : MonoBehaviour
	{
		[SerializeField]
		private Transform target;

		public float damping = 6.0f;
		public bool smooth = false;

		float delayTime;

		void Start ()
		{
			// Make the rigid body not change rotation
			if (GetComponent<Rigidbody> () != null)
				GetComponent<Rigidbody> ().freezeRotation = true;
		}

		public void init (Transform mTarget, float delayTime = 0)
		{
			this.delayTime = delayTime;
			this.target = mTarget;
			enabled = true;
		}

		public void initLookAt (Transform mTarget)
		{
			init (mTarget);
			transform.LookAt (target);
		}


		void LateUpdate ()
		{
			if (delayTime > 0) {
				delayTime -= Time.deltaTime;
				return;
			}

			if (target) {
				if (smooth) {
					var rotation = Quaternion.LookRotation (target.position - transform.position);
					transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime * damping);
				} else {
					// Just lookat
					transform.LookAt (target);
				}
			}
		}

		void OnDisable ()
		{
			enabled = false;
		}
	}
}