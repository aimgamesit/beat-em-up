﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace com.aquimo.util
{
	public class CallBackOnLastIndex : MonoBehaviour
	{
		public UnityEvent gameObjectEvent;

		bool isUpdated;

		//OPT//TEMP
		void Update ()
		{
			if (transform.isLastSibling ()) {
				if (!isUpdated) {
					gameObjectEvent.Invoke ();
				}
				isUpdated = true;
			} else {
				isUpdated = false;
			}
		}
	}
}