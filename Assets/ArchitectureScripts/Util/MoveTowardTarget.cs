﻿using UnityEngine;
using System.Collections;

namespace com.aquimo.util
{
	public class MoveTowardTarget : MonoBehaviour
	{
		Vector3 target;

		float speed;

		public void updateTarget (Vector3 target, float time = 0.25f)
		{
			enabled = true;
			this.target = target;
			float value = 1.0f / time;
			speed = Vector3.Distance (target, transform.position) * value;
		}
	
		// Update is called once per frame
		void LateUpdate ()
		{
			transform.position = Vector3.MoveTowards (transform.position, target, speed * Time.deltaTime);

			if (Vector3.Distance (transform.position, target) <= 0.01f) {
				enabled = false;
			}
		}
	}
}