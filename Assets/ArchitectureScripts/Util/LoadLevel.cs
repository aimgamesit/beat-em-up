using UnityEngine;
using UnityEngine.SceneManagement;

namespace com.aquimo.util
{
	public class LoadLevel : MonoBehaviour
	{
		public LevelType lvlType = LevelType.Index;
		//

		[Header ("Implement ILoadLevel")]
		public ScriptableObject
			levelIndexObj;
		// OPT

		public int levelIndex = 0;
		//
		public string levelName = "";
		//

		public bool loadOnStart;

		public string currentState;

		public void Start ()
		{
			if (loadOnStart)
				loadLevel ();
		}

		public void loadLevel ()
		{
			switch (lvlType) {
			case LevelType.Index:
				int index = (levelIndexObj && (levelIndexObj is ILoadLevel) ? ((ILoadLevel)levelIndexObj).getLevelIndex (currentState) : levelIndex);
				SceneManager.LoadScene (index);
				break;

			case LevelType.Name:
				Time.timeScale = 1;
				SceneManager.LoadScene (name);
				break;
			}

		}

	}

	public interface ILoadLevel
	{
		int getLevelIndex (string currentState);
	}

	public enum LevelType
	{
		Index,
		Name,
	}

}