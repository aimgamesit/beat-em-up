﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelInfo : MonoBehaviour
{
    public Image levelImage;
    public Text levelName;
    public Button clickButton;
    internal int levelId;
}