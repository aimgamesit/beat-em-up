﻿using UnityEngine;
using com.game.util;
using com.TicTok.managers;

[RequireComponent (typeof(EnvironmentZone))]
public partial class EnvironmentManager : BManager<EnvironmentManager>
{
	EnvironmentZone _mZone;

	public EnvironmentZone mZone {
		get {
			if (_mZone == null)
				_mZone = GetComponent<EnvironmentZone> ();
			return _mZone;
		}
	}

	public void updateEnvironment ()
	{
	}





}
