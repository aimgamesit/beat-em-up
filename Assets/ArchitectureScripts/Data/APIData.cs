﻿using UnityEngine;
using com.aquimo.data;
using com.aquimo.football.util;

namespace com.aquimo.football.api
{
	#region Load Data from resources folder and if not create asset file .
	public partial class APIData : BData<APIData>
	{
		

		public override string getResrcPath ()
		{
			return ResrcPath.API_DATA;
		}
			
		#if UNITY_EDITOR

	

		[UnityEditor.MenuItem (MenuItemPath.API_DATA)]
		public static void Edit ()
		{
			UnityEditor.Selection.activeObject = Instance;
		}
		#endif

	}
	#endregion

	public partial class APIData
	{
		public BaseURL baseURL;

		[SerializeField]
		string
			_PayLoadKey = "L3tTh3G4m3b3g1nW1thZ3pP3l1n";

		public string payLoadKey {
			get {
				return _PayLoadKey;
			}
		}

		public string version;

		public string appCode;

		public string profileType;

		public NetworkType networkType;

		[HideInInspector]
		public bool isAdmin;

	}

	public enum BaseURL : byte
	{
		DEV = 0,
		PROD = 1,
		Local = 2
	}

	public enum UserLoginType
	{
		Guest = 0,
		FB = 1,
		Aquimo = 2
	}

}

