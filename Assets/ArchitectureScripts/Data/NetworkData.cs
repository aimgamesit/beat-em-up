using UnityEngine;
using com.aquimo.util;
using com.aquimo.football.util;

namespace com.aquimo.data
{
	/// <summary>
	/// Class maintain common network related information in all games.
	/// </summary>
	public partial class NetworkData : BData<NetworkData>
	{
		public override string getResrcPath ()
		{
			return ResrcPath.Aquimo_DATA;
		}

		#if UNITY_EDITOR
		[UnityEditor.MenuItem(MenuItemPath.TICTOK_NETWORK_DATA)]
		public static void Edit ()
		{
			UnityEditor.Selection.activeObject = Instance;
		}
		#endif
	}

	public partial class NetworkData
	{
		public string internetCheckingUrl = "https://www.google.com";//

		[Tooltip("In Seconds")]
		public int
			requestTimeOut = 10;

		[Range(30,180)]
		public int
			recoveryAllowanceTime = 120;

		[Range(10,180)]  
		public int
			sendRefreshRate = 10 ;

	}
	public enum NetworkType :byte
	{
		Online = 0,
		Offline = 1
	}
}