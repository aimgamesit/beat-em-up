using UnityEngine;
using com.aquimo.util;

// Base class of all monobehaviour Controller scripts.
namespace com.TicTok.Controllers
{
	public abstract class BController<T> : MonoBehaviour ,IController where T : class
	{
		#region SingleTon Pattern.

		protected static T _Instance;

		public static T Instance {
			get {
				return _Instance;
			}
		}

		public virtual void Awake ()
		{
			_Instance = this as T;
		}

		#endregion
	}
}

