using UnityEngine;
using com.aquimo.util;

// Base class of all monobehaviour managers scripts.
namespace com.TicTok.managers
{
	public abstract class BManager<T,U> : BManager<T> where T : class where U : class, IZone
	{
		U _mZone;

		public U mZone {
			get {
				if (_mZone == null)
					_mZone = GetComponent<U> ();
				return _mZone;
			}
		}
	}

	public abstract class BManager<T> : MonoBehaviour ,IManager where T : class
	{
		#region SingleTon Pattern.

		protected static T _Instance;

		public static T Instance {
			get {
				return _Instance;
			}
		}

		public virtual void Awake ()
		{
			_Instance = this as T;
		}

		#endregion
	}


	public abstract class BManager : MonoBehaviour ,IManager
	{
	
	}
}

