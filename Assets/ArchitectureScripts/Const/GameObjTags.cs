﻿using com.aquimo.util;

namespace com.aquimo.football.util
{
	public static class GameObjTags
	{
		public const string CUE_HOLDER = "CueHolder";
		public const string BALL = "Ball";
		public const string TARGET = "Target";
		public const string BAT = "Bat";
	}
}