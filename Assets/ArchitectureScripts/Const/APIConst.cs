using UnityEngine;
using System.Text;
using com.aquimo.util;
using System.Collections;

namespace com.aquimo.football.api
{
	public partial class APIConst
	{
		#region Base Url

		const string LOCAL_URL = "http://americanfootball-api-dev.us-east-1.elasticbeanstalk.com/baseball/";
		const string PROD_URL = "http://americanfootball-api-prod.us-east-1.elasticbeanstalk.com/";
		const string DEV_URL = "http://americanfootball-api-dev.us-east-1.elasticbeanstalk.com/fbcan/";
		// "http://americanfootball-api-dev.us-east-1.elasticbeanstalk.com/baseball/";

		static string _BASE_URL;

		public static string BASE_URL {
			get {
				if (!string.IsNullOrEmpty (_BASE_URL))
					return _BASE_URL;

				_BASE_URL = LOCAL_URL;
				if (APIData.Instance.baseURL == BaseURL.PROD)
					_BASE_URL = PROD_URL;
				else if (APIData.Instance.baseURL == BaseURL.DEV)
					_BASE_URL = DEV_URL;

				return _BASE_URL;
			}
		}

		#endregion

		#region Key & IV

		static byte[] _IV;

		public static byte[] IV {
			get {
				string mIV = "YzB0MXggS2RPeCAmNGgwdA==";
				_IV = _IV ?? Encoding.UTF8.GetBytes (System.Text.Encoding.UTF8.GetString (System.Convert.FromBase64String (mIV)));
				return _IV;
			}
		}

		static byte[] _KEY;
		//
		public static byte[] KEY {
			get {
				string mKey = "VGgxcyA8OTMgdzByRiB2M3IgazFuZzUgIzFuRF8gJCQ=";
				_KEY = _KEY ?? Encoding.UTF8.GetBytes (System.Text.Encoding.UTF8.GetString (System.Convert.FromBase64String (mKey)));
				return _KEY;
			}
		}
		//

		#endregion

		#region Send Url

		public static string REGISTERATION_URL {
			get {
				return BASE_URL + "u/reg";
			}
		}

		public static string LOGIN_URL {
			get {
				return BASE_URL + "u/login";
			}
		}

		public static string LEADERBOARD_URL {
			get {
				return BASE_URL + "adm/leaderboard";
			}
		}

		public static string SCORE_URL {
			get {
				return BASE_URL + "u/score";
			}
		}

		public static string DASHBOARD_URL {
			get {
				return BASE_URL + "adm/dash";
			}
		}

		public static string LIVEEVENTINFO_URL {
			get {
				return BASE_URL + "e/events";
			}
		}

		public static string PUSH_NOTIFICATION_URL {
			get {
				return BASE_URL + "adm/u/token";
			}
		}


		public static string FORGOT_PASSWORD_URL {
			get {
				return BASE_URL + "u/forgotpassword";
			}
		}


		public static string RESEND_ACTIVATION_URL {
			get {
				return BASE_URL + "u/resendactivation";
			}
		}

		public static string SYNC_DATA_URL {
			get {
				return BASE_URL + "adm/dash/u/sync";//TODO change 
			}
		}

		public new static string ToString ()
		{
			return string.Format ("[APIConst: LOCAL_URL={0}, PROD_URL={1}, BASE_URL={2}, REGISTERATION_URL={3}, LOGIN_URL={4}," +
			"DASHBOARD_URL={5}, FORGOT_PASSWORD_URL={6}, RESEND_ACTIVATION_URL={7}]",
				LOCAL_URL, PROD_URL, BASE_URL, REGISTERATION_URL, LOGIN_URL,
				DASHBOARD_URL, FORGOT_PASSWORD_URL, RESEND_ACTIVATION_URL);
		}

		#endregion

		//Encrupt work
		public static class Encrypt
		{
			static string _APP_REQUEST;

			public static string APP_REQUEST {
				get {
					if (_APP_REQUEST == null)
						_APP_REQUEST = EncryptionHandler.EncryptIt ("AppRequest");

					return _APP_REQUEST;
				}
			}

			static string _DEVICE_ID;

			public static string DEVICE_ID {
				get {
					if (_DEVICE_ID == null)
						_DEVICE_ID = EncryptionHandler.EncryptIt (SystemInfo.deviceUniqueIdentifier);
					return _DEVICE_ID;
				}
			}

			static string _PAYLOAD_KEY;

			public static string PAYLOAD_KEY {
				get {
					if (_PAYLOAD_KEY == null)
						_PAYLOAD_KEY = EncryptionHandler.EncryptIt (APIData.Instance.payLoadKey);
					return _PAYLOAD_KEY;
				}
			}
		}
	}

	public class APIPrefsKey : BPrefs
	{
		public const string LOGIN_PASSWORD = "Login_Password";

		public const string PLAYER_INFO = "Player_Info";
		public const string LEADERBOARD_INFO = "Leaderboard_Info";
		public const string LAST_EVENT_NAME = "Last_Event_Name";
		public const string IMAGE_URL = "User_ID";
		public const string PUSH_NOTIFICATION_STATUS = "push_notification_status";

		public const string USER_NAME = "User_Name";
		public const string DISPLAY_NAME = "display_Name";
		public const string USER_PASS = "User_Pass";
		public const string STICKLPOOL_NAME = "Stickpool_Name";
	}

	public class APIFileName
	{
		public const string PLAYER_INFO = "PlayerInfo";
		public const string LEADERBOARD_INFO = "LeaderboardInfo";
		public const string LIVEEVENT_INFO = "LiveEventInfo";
	}

	public class APIStatusCode
	{
		public const int SUCCESSFUL = 1;
	}


}