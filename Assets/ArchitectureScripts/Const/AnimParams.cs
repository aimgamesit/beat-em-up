﻿namespace com.aquimo.football.util
{
	
	public static class AnimParams
	{
		public const string THROW = "throw";

		public const string HIT = "Hit";

		public const string PITCH = "Pitch";
	}
}
