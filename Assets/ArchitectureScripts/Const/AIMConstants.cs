﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMConstants : MonoBehaviour
{
    public const string ASSET_PARENT = "Scripts";
    public const string RESOURCES_PATH = "AIMAssets/" + ASSET_PARENT + "/Data";
    public const string GAME_DATA_MENU = "AIMGameInfo/GameData";
    public const string PLAYER_DATA_MENU = "AIMGameInfo/HoldData";
}