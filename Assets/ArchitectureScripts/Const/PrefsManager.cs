﻿using com.TicTok.managers;
using UnityEngine;

public class PrefsManager : BManager<PrefsManager>
{
    #region Prefabs Key's
    public const string PLAYER_INDEX = "player_index";

    #endregion

    #region Get or Set Functions
    public void SetInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
        PlayerPrefs.Save();
    }
    public int GetInt(string key, int defaultValue = 0) => PlayerPrefs.GetInt(key, defaultValue);

    public void SetFloat(string key, float value)
    {
        PlayerPrefs.SetFloat(key, value);
        PlayerPrefs.Save();
    }
    public float GetFloat(string key, float defaultValue = 0.0f) => PlayerPrefs.GetFloat(key, defaultValue);

    public void SetString(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
        PlayerPrefs.Save();
    }
    public string GetString(string key, string defaultValue = "") => PlayerPrefs.GetString(key, defaultValue);
    #endregion
}