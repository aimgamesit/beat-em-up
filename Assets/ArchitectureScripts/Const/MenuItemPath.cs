﻿using UnityEngine;
using System.Collections;

namespace com.aquimo.football.util
{
public class MenuItemPath : MonoBehaviour {


	public const string TICTOK_PLAY_DATA= "TicTok/PlayData";
	public const string RT_EVENT_DATA= "TicTok/RTEventData";
	public const string TICTOK_NETWORK_DATA= "TicTok/Network";
	public const string API_DATA = "API/APIData";
    public const string PLAYER_INFO = "GAMEINFO/PlayerInfo";
	public const string OPPONENT_INFO = "GAMEINFO/OpponentInfo";
	public const string GAME_DATA = "GAMEINFO/GameData";
	public const string OFFLINE_DATA = "GAMEINFO/OfflineData";
	public const string SETTING_DATA = "GAMEINFO/SettingData";
	public const string GAME_PLAY_STATE_DATA = "GAMEINFO/GamePlayState";
	public const string POKER_GAME_DATA = "GAMEINFO/Poker/GameData";
	public const string POKER_SETTING_DATA = "GAMEINFO/Poker/SettingData";


}
}