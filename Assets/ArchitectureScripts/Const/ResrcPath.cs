﻿using System.IO;
using com.aquimo.util;

namespace com.aquimo.football.util
{
	// Analysis disable once ConvertToStaticType
	public class ResrcPath :  IConst
	{
		
		public const string Play_DATA = "Test/Script/Data";
		public const string Aquimo_DATA = "DefaultScripts/Data";
		public const string API_DATA = "DefaultScripts/Data";
		public const string Bowling_DATA = "Scripts/Data";
		public const string PLAYER_INFO_DATA = "Scripts/Data";
		public const string OPPONENT_INFO_DATA = "Scripts/Data";
		public const string RT_EVENT_DATA = "Scripts/Data";
		public const string GAME_DATA = "Scripts/Data";
		public const string OFFLINE_DATA = "Scripts/Data";
		public const string SETTING_DATA = "Scripts/Data";
		public const string GAME_PLAY_STATE_DATA = "Scripts/Data";
		public const string POKER_SETTING_DATA = "_SP_Poker/Scripts/Data";
        public const string POKER_GAME_DATA = "_SP_Poker/Scripts/Data";


    }



}