﻿using com.aquimo.data;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

#region Load Data from resources folder and if not create asset file.
public partial class AIMGameData : BData<AIMGameData>
{
    public override string getResrcPath()
    {
        return AIMConstants.RESOURCES_PATH;
    }
#if UNITY_EDITOR
    [UnityEditor.MenuItem(AIMConstants.GAME_DATA_MENU)]

    public static void Edit()
    {
        UnityEditor.Selection.activeObject = Instance;
    }
#endif
}
#endregion

#region Global Variable Decleration
public partial class AIMGameData
{
    public GameConfig gameConfiguration;
    public List<CharacterData> playerCharList = new List<CharacterData>();
    public List<CharacterData> enemyCharList = new List<CharacterData>();
    public AdsConfig adsConfiguration;
}
#endregion

#region Serializable Classes
public partial class AIMGameData
{
    [Serializable]
    public class GameConfig
    {
        [Header("Scenes Name")]
        public string menuSceneName = "";
        public string gameSceneName = "";
        public string trainingSceneName = "";
        [Header("Application Settings")]
        public float timeScale = 1f;
        public int framerate = 60;
        public bool showFPSCounter = false;

        [Header("Audio Settings")]
        public float MusicVolume = .7f;
        public float SFXVolume = .9f;
    }

    [Serializable]
    public class AdsConfig
    {
        public bool isTestMode = false;
        public bool isTopBanner = false;
        public string AppId = "";
        public string InterstialId = "";
        public string BannerId = "";
        public string RewardedVideoId = "";
    }
    [Serializable]
    public class CharacterData
    {
        public Material skinMat;
        public Material clothMat;
        public Sprite charSprite;
        public GameObject charPrefab;
    }
}
#endregion