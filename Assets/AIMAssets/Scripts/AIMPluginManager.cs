﻿using UnityEngine;
using System.Collections;

public class AIMPluginManager : MonoBehaviour
{
	public static AIMPluginManager mInstance;
	private AndroidJavaClass mAndroidJavaClass;
	private AndroidJavaObject mAndroidJavaObject, mAndroidJavaObjectActivity;

	void Awake ()
	{
		//DontDestroyOnLoad (this);
		mInstance = this;
	}

	void Start ()
	{
		InitializeStart ();
	}

	private void InitializeStart ()
	{
		mAndroidJavaClass = new AndroidJavaClass ("com.unity3d.player.UnityPlayer"); 
		mAndroidJavaObject = new AndroidJavaObject ("com.aimsoftit.mylibrary.AIMUtility");
		mAndroidJavaObjectActivity = mAndroidJavaClass.GetStatic<AndroidJavaObject> ("currentActivity");
	}

	public void ShowShortToast (string aMsg)
	{
		mAndroidJavaObject.CallStatic ("showShortToast", mAndroidJavaObjectActivity, aMsg);
	}

	public void ShowLongToast (string aMsg)
	{
		mAndroidJavaObject.CallStatic ("showLongToast", mAndroidJavaObjectActivity, aMsg);
	}

	public void ShareApp (string aMessage)
	{
		mAndroidJavaObject.CallStatic ("shareApp", mAndroidJavaObjectActivity, aMessage);
	}

	public void RateUs ()
	{
		mAndroidJavaObject.CallStatic ("rateUs", mAndroidJavaObjectActivity);
	}
}

