﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryZone : MonoBehaviour
{
    public Transform playersContainer;
    public GameObject playerPrefab;
    public GameObject selectedCharObj;
    public SkinnedMeshRenderer skinnedMeshRenderer;
}