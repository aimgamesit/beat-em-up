﻿using com.game.util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIZone : BZone
{
    public EnemyWaveSystem enemyWaveSystem;
    public GameObject enemyInfoObj;
    public BoxCollider areaCollider;
    public InputManager inputManager;
    public CameraFollow cameraFollow;
    public WinnerScreenManager winnerScreenManager;
    public GameOverManager gameOverManager;
    public GameObject touchScreenControls;
}
