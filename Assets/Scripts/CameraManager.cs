﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    #region Value
    public Camera mainCamera;
    public FixedTouchField fixedTouchField;
    public float MouseSpeedX = 0.06f;
    public float MouseSpeedY = 0.01f;
    public float FollowSpeed = 9;
    public float TurnSmoothing = 0.1f;
    public float MinAngle = -35f;
    public float MaxAngle = 35f;
    public Vector3 standUpPosition = Vector3.zero;
    public Vector3 crouchPosition = Vector3.zero;
    #endregion

    public Transform TargetTf;

    private Transform _pivotTf;
    private Transform _camTf;

    public float _smoothX;
    private float _smoothXvelocity;
    private float _smoothY;
    private float _smoothYvelocity;
    private float _lookAngle;
    public float _tiltAngle;

    void Start()
    {
        // Init();
    }

    public void Init(Transform TargetTf)
    {
        this.TargetTf = TargetTf;
        _camTf = mainCamera.transform;
        _pivotTf = _camTf.parent;
    }


    void Update()
    {
        if (TargetTf == null)
            return;
        Tick(Time.deltaTime);
    }

    public void Tick(float deltaTime)
    {
        FollowTarget(deltaTime);
        HandleRotations(fixedTouchField.TouchDist.y, fixedTouchField.TouchDist.x, MouseSpeedX, MouseSpeedY);
    }

    private void FollowTarget(float deltaTime)
    {
        Vector3 newPos = Vector3.Lerp(transform.position, TargetTf.position, deltaTime * FollowSpeed);
        transform.position = newPos;
    }

    private void HandleRotations(float vertical, float horizontal, float rotationSpeedX, float rotationSpeedY)
    {
        if (TurnSmoothing > 0f)
        {
            _smoothX = Mathf.SmoothDamp(_smoothX, horizontal, ref _smoothXvelocity, TurnSmoothing);
            _smoothY = Mathf.SmoothDamp(_smoothY, vertical, ref _smoothYvelocity, TurnSmoothing);
        }
        else
        {
            _smoothX = horizontal;
            _smoothY = vertical;
        }
        _lookAngle += _smoothX * rotationSpeedX;
        transform.localRotation = Quaternion.Euler(0, _lookAngle, 0);
        TargetTf.localRotation = Quaternion.Euler(0, _lookAngle, 0);

        _tiltAngle -= _smoothY * rotationSpeedY;
        _tiltAngle = Mathf.Clamp(_tiltAngle, MinAngle, MaxAngle);
        _pivotTf.localRotation = Quaternion.Euler(_tiltAngle, 0, 0);
    }

    public void OnStandUp()
    {
        mainCamera.gameObject.transform.localPosition = standUpPosition;
    }

    public void OnCrouch()
    {
        mainCamera.gameObject.transform.localPosition = crouchPosition;
    }
}