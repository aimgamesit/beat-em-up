﻿using UnityEngine;
using com.TicTok.managers;

[RequireComponent(typeof(UIZone))]
public partial class UIManager : BManager<UIManager>
{
    UIZone _MZone;
    public UIZone mZone
    {
        get
        {
            if (_MZone == null)
            {
                _MZone = this.GetComponent<UIZone>();
            }
            return _MZone;
        }
    }
}

public partial class UIManager {
	void Start(){
        Time.timeScale = AIMGameData.Instance.gameConfiguration.timeScale;
        Application.targetFrameRate = AIMGameData.Instance.gameConfiguration.framerate;
        mZone.touchScreenControls.SetActive(mZone.inputManager.inputType == INPUTTYPE.TOUCHSCREEN);
        mZone.enemyInfoObj.SetActive(GamePlayManager.Instance.holdDataInfo.isPlayWithOneVsOne);

        SetEnemyData();
    }

    public void SetEnemyData() {
        mZone.enemyWaveSystem.currentWave = 0;
        mZone.enemyWaveSystem.InstentiateNewEnemies();
        mZone.enemyWaveSystem.UpdateAreaColliders();
        mZone.enemyWaveSystem.SetEnemyActionsEvent();
        mZone.enemyWaveSystem.StartNewWave();
    }
}