﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(TrainingZone))]
public partial class TrainingManager : MonoBehaviour
{
    TrainingZone _MZone;
    public TrainingZone mZone
    {
        get
        {
            if (_MZone == null)
            {
                _MZone = this.GetComponent<TrainingZone>();
            }
            return _MZone;
        }
    }
}

public partial class TrainingManager
{
    void Start() {
        mZone.touchScreenControls.SetActive(mZone.inputManager.inputType == INPUTTYPE.TOUCHSCREEN);
    }
    public void OnBackButtonClicked()
    {
        SceneManager.LoadScene(AIMGameData.Instance.gameConfiguration.menuSceneName);
    }
}
