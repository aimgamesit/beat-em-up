﻿using com.TicTok.managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MenuUIZone))]
public partial class MenuUIManager : BManager<MenuUIManager>
{
    MenuUIZone _MZone;
    public MenuUIZone mZone
    {
        get
        {
            if (_MZone == null)
            {
                _MZone = this.GetComponent<MenuUIZone>();
            }
            return _MZone;
        }
    }
}
