﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(GameOverZone))]
public partial class GameOverManager : MonoBehaviour
{
    GameOverZone _MZone;
    public GameOverZone mZone
    {
        get
        {
            if (_MZone == null)
            {
                _MZone = this.GetComponent<GameOverZone>();
            }
            return _MZone;
        }
    }
}

public partial class GameOverManager : MonoBehaviour
{
    void Start()
    {

    }

    public void OnMenuButtonCLicked() {
        SceneManager.LoadScene("Menu");
    }
}