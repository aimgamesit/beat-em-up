﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(WinnerScreenZone))]
public partial class WinnerScreenManager : MonoBehaviour
{
    WinnerScreenZone _MZone;
    public WinnerScreenZone mZone
    {
        get
        {
            if (_MZone == null)
            {
                _MZone = this.GetComponent<WinnerScreenZone>();
            }
            return _MZone;
        }
    }
}
public partial class WinnerScreenManager
{
    void Start()
    {

    }
    public void OnMenuButtonCLicked()
    {
        SceneManager.LoadScene("Menu");
    }

}