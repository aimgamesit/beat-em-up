﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(MainMenuZone))]
public partial class MainMenuManager : MonoBehaviour
{
    MainMenuZone _MZone;
    public MainMenuZone mZone
    {
        get
        {
            if (_MZone == null)
            {
                _MZone = this.GetComponent<MainMenuZone>();
            }
            return _MZone;
        }
    }
}
public partial class MainMenuManager
{
    #region Button click event
    public void OnPlayGameBtnClicked()
    {
        SceneManager.LoadScene(AIMGameData.Instance.gameConfiguration.gameSceneName);
    }
    public void OnInventoryBtnClicked()
    {
        MenuUIManager.Instance.mZone.playerSelectionManager.SetActive(true);
    }

    #endregion
}