﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static AIMGameData;

[RequireComponent(typeof(InventoryZone))]
public class InventoryManager : MonoBehaviour
{
    InventoryZone _MZone;
    public InventoryZone mZone
    {
        get
        {
            if (_MZone == null)
            {
                _MZone = this.GetComponent<InventoryZone>();
            }
            return _MZone;
        }
    }

    #region Button click event
    public void OnBackButtonClicked()
    {
        gameObject.SetActive(false);
    }
    void OnEnable()
    {
        SetCharData(PrefsManager.Instance.GetInt(PrefsManager.PLAYER_INDEX, 0));
        SetData();
    }

    public void SetData()
    {
        mZone.selectedCharObj.transform.SetParent(transform);
        mZone.playersContainer.DestroyChilds();
        AIMGameData aimGameData = AIMGameData.Instance;
        for (int ind = 0; ind < aimGameData.playerCharList.Count; ind++)
        {
            GameObject gameObj = Instantiate(mZone.playerPrefab, mZone.playersContainer);
            AvatarInfo avatarInfo = gameObj.GetComponent<AvatarInfo>();
            avatarInfo.charIMG.sprite = aimGameData.playerCharList[ind].charSprite;
            avatarInfo.charIndex = ind;
            avatarInfo.button.onClick.AddListener(() => { OnPlayerClicked(avatarInfo); });
        }
    }

    public void OnPlayerClicked(AvatarInfo avatarInfo)
    {
        Debug.Log("Player Index:" + avatarInfo.charIndex);
        mZone.selectedCharObj.transform.SetParent(avatarInfo.gameObject.transform);
        PrefsManager.Instance.SetInt(PrefsManager.PLAYER_INDEX, avatarInfo.charIndex);
        SetCharData(avatarInfo.charIndex);
    }

    private void SetCharData(int charIndex)
    {
        AIMGameData aimGameData = AIMGameData.Instance;
        mZone.skinnedMeshRenderer.materials[0] = aimGameData.playerCharList[charIndex].clothMat;
        mZone.skinnedMeshRenderer.materials[1] = aimGameData.playerCharList[charIndex].skinMat;
    }
    #endregion
}